import re

with open('input1.txt', 'r') as file:
    input = file.read()
    split = input.split()

digits = [re.findall("\d", i) for i in split]
print("Challenge 1: ", sum(int(i[0] + i[-1]) for i in digits))

converted = re.sub(r"one", "o1e", input)
converted = re.sub(r"two", "t2o", converted)
converted = re.sub(r"three", "t3e", converted)
converted = re.sub(r"four", "f4r", converted)
converted = re.sub(r"five", "f5e", converted)
converted = re.sub(r"six", "s6x", converted)
converted = re.sub(r"seven", "s7n", converted)
converted = re.sub(r"eight", "e8t", converted)
converted = re.sub(r"nine", "n9e", converted)
converted = re.sub(r"ten", "t10n", converted)

split = converted.split()

digits = [re.findall("\d", i) for i in split]

print("Challenge 2: ", sum(int(i[0] + i[-1]) for i in digits))

file.close()